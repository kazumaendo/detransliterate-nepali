import pandas as pd
import os
from translator import detransliterate_nepali
import nepali_roman as nr
os.chdir('/Users/Kazuma 1/Downloads')
dataset1 = pd.read_csv('maya_transliteration_dataset.csv')

import numpy
def printDistances(distances, token1Length, token2Length):
    for t1 in range(token1Length + 1):
        for t2 in range(token2Length + 1):
            print(int(distances[t1][t2]), end=" ")
        print()
def levenshteinDistanceDP(token1, token2):
    distances = numpy.zeros((len(token1) + 1, len(token2) + 1))

    for t1 in range(len(token1) + 1):
        distances[t1][0] = t1

    for t2 in range(len(token2) + 1):
        distances[0][t2] = t2
        
    a = 0
    b = 0
    c = 0
    
    for t1 in range(1, len(token1) + 1):
        for t2 in range(1, len(token2) + 1):
            if (token1[t1-1] == token2[t2-1]):
                distances[t1][t2] = distances[t1 - 1][t2 - 1]
            else:
                a = distances[t1][t2 - 1]
                b = distances[t1 - 1][t2]
                c = distances[t1 - 1][t2 - 1]
                
                if (a <= b and a <= c):
                    distances[t1][t2] = a + 1
                elif (b <= a and b <= c):
                    distances[t1][t2] = b + 1
                else:
                    distances[t1][t2] = c + 1

    return distances[len(token1)][len(token2)]

import fuzzy
soundex = fuzzy.Soundex(4)

count=0

for transliterated_text, devangari_text in zip(dataset1['transliterated_sentence'], dataset1['devangari_sentence']):
    pred_text = detransliterate_nepali(text=transliterated_text)
    pred_roman = nr.romanize_text(pred_text).replace(" ","")
    devangari_roman = nr.romanize_text(devangari_text).replace(" ","")

    pred_roman_mod = ''.join(c for c in pred_roman if c.isalpha())
    devangari_roman_mod = ''.join(c for c in devangari_roman if c.isalpha())

    try:
        if soundex(pred_roman_mod) == soundex(devangari_roman_mod):
            count+=1
    except UnicodeEncodeError:
        continue
    except UnicodeDecodeError:
        continue

accuracy_score = count / dataset1.shape[0]
print(accuracy_score)