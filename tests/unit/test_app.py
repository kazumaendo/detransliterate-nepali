import requests
import json
import os
import sys
from flask import Flask

import pytest

script_dir = os.path.dirname( __file__ )
mymodule_dir = os.path.join( script_dir, '..', '..')
sys.path.append( mymodule_dir )
from webapp import api_message
from webapp import app

#API URL
url = url = "http://127.0.0.1:5000/detransliterate-nepali"

@pytest.fixture(scope='module')
def test_client():

    app.config['TESTING'] = True  # disables error catching during request handling to get better error reports
    testing_client = app.test_client()
    ctx = app.app_context()
    ctx.push()

    yield testing_client

    ctx.pop()

def test_post_request():
    file = open("detransliterate_request.json")
    json_input = file.read()
    request_json= json.loads(json_input)
    response = requests.post(url, request_json)
    assert response.status_code == 201
    response_json = response.json()
    print(response_json)
