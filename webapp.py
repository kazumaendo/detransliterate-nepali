from flask import Flask, json, request, jsonify
from translator import detransliterate_nepali
app = Flask(__name__)
@app.route('/detransliterate-nepali', methods=['POST'])

def api_message():
    if request.headers['Content-Type'] == 'application/json':
        request_data = request.get_json()
        request_text = request_data['text']
        translated = detransliterate_nepali(text=request_text)
        return jsonify({'devangari':translated})

    else:
        return "415 Unsupported Media Type ;)"

if __name__ == '__main__':
    app.run()